Rails.application.routes.draw do
  # resources :reponses

  # resources :annonces

  resources :residences

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords',
    confirmations: 'users/confirmations',
    unlocks: 'users/unlocks'
  }, path: 'users', path_names: {sign_in: 'login', sign_out: 'logout', sign_up: 'register'}

  resources :annonces do
    resources :reponses do
    end
  end

  # static pages and links
  root 'main#index'
  get '/termes', to: 'main#termes'
  get '/politique', to: 'main#politique'
  get '/contact', to: 'main#contact'

  get 'users/:id/annonces' => 'annonces#index', :as => :user_annonces
  get '/users/:id',   to: 'users#show', :as => :user_profil

  # error pages
  %w( 404 422 500 503 ).each do |code|
    get code, :to => "errors#show", :code => code
  end

  #If no route matches
  get ":url" => "application#redirect_user", :constraints => { :url => /.*/ }
end
