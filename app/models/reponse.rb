class Reponse < ActiveRecord::Base
  belongs_to :annonce
  belongs_to :user

  validates :Contenu, presence: true
end
