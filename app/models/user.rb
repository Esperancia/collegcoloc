class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :cin, :styles => { :medium => "300x300>", :thumb => "100x100>" }, default_url: "1.jpg",
   :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] },
   :path => ":rails_root/public/cin/:id/:style/:basename.:extension",
   :url => ":rails_root/public/cin/:id/:style/:basename.:extension"

  # validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  has_many :annonces, :dependent => :destroy

  has_many :reponses, :dependent => :destroy
end
