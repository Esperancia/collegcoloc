class Residence < ActiveRecord::Base
  has_attached_file :Apercu, default_url: "2.jpg"
  validates_attachment :Apercu, :styles => { :medium => "100x100>", :thumb => "64x64>" },
   :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] },
   :path => ":rails_root/public/residences/:id/:basename.:extension",
   :url => ":rails_root/public/residences/:id/:basename.:extension"

  # validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates :Titre, :Description, presence: true
end
