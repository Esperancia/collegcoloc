class Annonce < ActiveRecord::Base
  belongs_to :user
  has_many :reponses

  has_attached_file :Apercu, default_url: "1.jpg"
  validates_attachment :Apercu, :styles => { :medium => "300x300>", :thumb => "100x100>" },
   :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"]  },
   :path => "/public/:annonces/:basename.:extension",
   :url => "/:annonces/:basename.:extension",
   :default_url => 'public/annonces/1.png'

  # validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates :Titre, :Description, :Type, :Budget, :Adresse, :DateCloture, :user_id, presence: true
  validates :Budget, :numericality => {:only_integer => true}

  acts_as_commentable :public, :private, { class_name: 'Reponse' }
end
