class MainController < ApplicationController
  def index
    @annonces = Annonce.all
    @residences = Residence.all
  end

  def comment
  end

  def apropos
  end

  def termes
  end

  def administration
  end
end
