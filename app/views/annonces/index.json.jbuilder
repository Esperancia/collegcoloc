json.array!(@annonces) do |annonce|
  json.extract! annonce, :id, :Titre, :Description, :Apercu, :Type, :Adresse, :DateCloture
  json.url annonce_url(annonce, format: :json)
end
