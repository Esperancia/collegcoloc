json.array!(@residences) do |residence|
  json.extract! residence, :id, :Titre, :Description, :Apercu
  json.url residence_url(residence, format: :json)
end
