class CreateReponses < ActiveRecord::Migration
  def change
    create_table :reponses do |t|
      t.text :Contenu
      t.integer :annonce_id
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :reponses, :annonce_id
    add_index :reponses, :user_id
  end
end
