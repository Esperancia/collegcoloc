class CreateResidences < ActiveRecord::Migration
  def change
    create_table :residences do |t|
      t.string :Titre
      t.text :Description
      t.attachment :Apercu

      t.timestamps null: false
    end
  end
end
