class CreateAnnonces < ActiveRecord::Migration
  def change
    create_table :annonces do |t|
      t.string :Titre
      t.text :Description
      t.attachment :Apercu
      t.string :Type
      t.integer :Budget
      t.string :Adresse
      t.datetime :DateCloture
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :annonces, :user_id
  end
end
