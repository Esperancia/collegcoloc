# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create([
        { email: 'champouxah@yahoo.fr', nom: "IDRIS", prenom: 'Hadjara', password: "azertyuiop", sexe: "Féminin", contact: "Fidjrossè Cotonou BENIN" },
        { email: 'esperancia@tekxl.com', nom: "AHEHEHINNOU", prenom: 'Esperancia', password: "azertyuiop", sexe: "Féminin", contact: "Agla Cotonou BENIN" },
        { email: 'esperance@tekxl.com', nom: "ABDELAZIZ", prenom: 'Mashkour', password: "azertyuiop", sexe: "Masculin", contact: "Calavi BENIN" },
        { email: 'champouxa@yahoo.fr', nom: "KEMSET", prenom: 'Roy', password: "azertyuiop", sexe: "Masculin", contact: "Parakou BENIN" }
        ])

residence = Residence.create([
        {Titre: "Deux chambres un salon/agence BBT Immo", Description: "Deux chambres un salon/"},
        {Titre: "Cinq chambres un salon/agence 3T Immo", Description: "Cinq chambres un salon/"},
        {Titre: "Une chambre un salon/WKZ Immo", Description: ""},
        {Titre: "Trois chambres un salon/WKZ Immo", Description: "Trois chambres un salon/"},
        {Titre: "Quatre chambres un salon/agence BBT Immo", Description: "Quatre chambres un salon/"},
        {Titre: "Une chambre un salon/agence 3T Immo", Description: "Une chambre un salon/"}
  ])

#  :Titre, :Description, :Type, :Budget, :Adresse, :DateCloture, :user_id

annonce = Annonce.create([
        { Titre: "Une chambre un salon", Description: "Une chambre un salon", Type: "Location", Budget: "20000", Adresse: "Cotonou BENIN", DateCloture: "2015-11-23 14:13:10.934815", user_id: "1" },
        { Titre: "Deux chambres un salon", Description: "Deux chambres un salon", Type: "Colocation", Budget: "22000", Adresse: "SENEGAL", DateCloture: "2015-11-23 14:13:10.934815", user_id: "1" },
        { Titre: "Une chambre un salon", Description: "Une chambre un salon", Type: "Location", Budget: "20000", Adresse: "MALI", DateCloture: "2015-11-23 14:13:10.934815", user_id: "4" },
        { Titre: "Trois chambres un salon", Description: "Trois chambres un salon", Type: "Colocation", Budget: "30000", Adresse: "Cotonou BENIN", DateCloture: "2015-11-23 14:13:10.934815", user_id: "2" },
        { Titre: "Une chambre un salon", Description: "Une chambre un salon", Type: "Location", Budget: "15000", Adresse: "NIGER", DateCloture: "2015-11-23 14:13:10.934815", user_id: "1" },
        { Titre: "Deux chambres un salon", Description: "Deux chambres un salon", Type: "Colocation", Budget: "20000", Adresse: "COTE DIVOIRE", DateCloture: "2015-11-23 14:13:10.934815", user_id: "" },
        { Titre: "Quatre chambres un salon", Description: "Quatre chambres un salon", Type: "Location", Budget: "30000", Adresse: "Cotonou BENIN", DateCloture: "2015-11-23 14:13:10.934815", user_id: "2" },
        { Titre: "Une chambre un salon", Description: "Une chambre un salon", Type: "Location", Budget: "24000", Adresse: "MALI", DateCloture: "2015-11-23 14:13:10.934815", user_id: "3" },
        { Titre: "Deux chambres un salon", Description: "Deux chambres un salon", Type: "Colocation", Budget: "20000", Adresse: "Calavi BENIN", DateCloture: "2015-11-23 14:13:10.934815", user_id: "4" },
        { Titre: "Une chambre un salon", Description: "Une chambre un salon", Type: "Location", Budget: "25000", Adresse: "Parakou BENIN", DateCloture: "2015-11-23 14:13:10.934815", user_id: "3" }
  ])
